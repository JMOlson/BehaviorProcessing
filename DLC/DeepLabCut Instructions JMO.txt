# DeepLabCut Usage Notes - My fist attempt, Nov. 2022


## Gen Utilities Needed
ffmpeg for file conversion. In ubuntu:
sudo snap install ffmpeg
Then make sure it can see your drives:
sudo snap connect ffmpeg:removable-media


to observe training progress:
tensorboard --logdir '/media/jmolson/SpeedData/DataProcessing/dlc/GreenLeftRedRight/SubLearnTrack-JMO-2022-10-12/dlc-models/iteration-0/SubLearnTrackOct12-trainset95shuffle1/train/log'

## Ensure correct nvidia gpu and cuda drivers installed.
https://github.com/DeepLabCut/DeepLabCut/blob/master/docs/installation.md, GPU support section.
https://deeplabcut.github.io/DeepLabCut/docs/recipes/installTips.html#installation-on-ubuntu-20-04-lts

http://www.mackenziemathislab.org/deeplabcut
https://deeplabcut.github.io/DeepLabCut/docs/installation.html







## Find the mount point for citadel when using the GUI to mount:
Drag a folder in the folder into terminal, you'll get the address. it will look like this:
/run/user/1000/gvfs/smb-share:server=citadel.bio.brandeis.edu,share=data'

So assign that location to a variable, and cd there and work there like normal.
DataLoc='/run/user/1000/gvfs/smb-share:server=citadel.bio.brandeis.edu,share=data'
cd $DataLoc
cd Projects/jmo_SubLearning

## Gather files
Cycle through day recordings, copy over videos locally, in one folder
e.g. - following code, switch srchDir, repeat.
srchDir="$DataLoc/Projects/jmo_SubLearning/SL18/"
destDir="/mnt/nvme4TB/JMO/dlc/landing/"

for i in $(find $srchDir -name "*.h264"); do cp $i $destDir/; done





## Convert to mp4
https://trac.ffmpeg.org/wiki/Encode/H.264
ffmpeg -i ./SL06_D23_S02_F01_Home+4_HomeAltVisitAll_20211016_122822.1.h264 -c:v h264 -crf 18 -preset slow ./test.mp4

conversion rate settings - speeds are for 1280x920 video size
slow		 6.4x - 120% h264 file size
veryfast	14.1x - 105% !? use this one!
superfast	21.0x - 210%
ultrafast	30.5x - 250%

for i in $(find $destDir -name "*.h264"); do ffmpeg -i $i -c:v h264 -crf 18 -preset veryfast "${i%.*}.mp4"; done





Split - behavior no lights, sleep box, behavior w/lights


python -m deeplabcut

Add videos to analyze videos















