# DLC Usage
I ran it, and I can't find notes on install, usage, file conversion, nothing. FFS, wtf happened?


## Resource Files
Generally speaking, DLC provides up to date installation and usage instructions. They should be used, with guidance here superceding their instructions. Basically, I'll try to point out where problems occured and how I fixed them.

https://github.com/DeepLabCut/DeepLabCut


## Installation - works
Copy deeplabcut repo. Put in
dlcPath=/mnt/4TBData/Code\ Repos/DeepLabCut

conda create --name dlc python=3.8
conda activate dlc
pip install deeplabcut[tf,gui]

python $dlcPath/examples/testscript.py
Doesn't seem to run well, but the real thing does, so :shrug:


## File Gathering & Conversion

### Install converter
ffmpeg for file conversion. In ubuntu:
sudo snap install ffmpeg
Then make sure it can see your drives:
sudo snap connect ffmpeg:removable-media

### Gather files
Cycle through day recordings, move over videos.
e.g. - following code, switch srchDir, repeat.
srchDir="/media/jmolson/BigData_Internal_1/Data/SubLearning/SL06/SL06_D23/";
destDir="/media/jmolson/SpeedData/DataProcessing/dlc/landing"

for i in $(find $srchDir -name "*.h264"); do cp $i $destDir/; done

### Convert to mp4
https://trac.ffmpeg.org/wiki/Encode/H.264
ffmpeg -i ./SL06_D23_S02_F01_Home+4_HomeAltVisitAll_20211016_122822.1.h264 -c:v h264 -crf 18 -preset slow ./test.mp4

conversion rate settings - speeds are for 1280x920 video size
slow		 6.4x - 120% h264 file size
veryfast	14.1x - 105% !? use this one!
superfast	21.0x - 210%
ultrafast	30.5x - 250%

for i in $(find $destDir -name "*.h264"); do ffmpeg -i $i -c:v h264 -crf 18 -preset veryfast "${i%.*}.mp4"; done




























## Installation - Their instructions, doesn't work. Here for posterity.
https://github.com/DeepLabCut/DeepLabCut/blob/main/docs/installation.md
https://deeplabcut.github.io/DeepLabCut/docs/recipes/installTips.html#installation-on-ubuntu-20-04-lts

I followed the top instructions, and ran the test script,
python /mnt/4TBData/Code\ Repos/DeepLabCut/examples/testscript.py

then received the following error:
2023-01-31 12:08:37.583856: E tensorflow/stream_executor/cuda/cuda_blas.cc:2981] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
2023-01-31 12:08:38.016423: W tensorflow/stream_executor/platform/default/dso_loader.cc:64] Could not load dynamic library 'libnvinfer.so.7'; dlerror: libnvinfer.so.7: cannot open shared object file: No such file or directory
2023-01-31 12:08:38.016471: W tensorflow/stream_executor/platform/default/dso_loader.cc:64] Could not load dynamic library 'libnvinfer_plugin.so.7'; dlerror: libnvinfer_plugin.so.7: cannot open shared object file: No such file or directory
2023-01-31 12:08:38.016477: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Cannot dlopen some TensorRT libraries. If you would like to use Nvidia GPU with TensorRT, please make sure the missing libraries mentioned above are installed properly.

But evidently that's okay, it should just be a warning.

But then I had the following error:
qt.qpa.plugin: Could not find the Qt platform plugin "xcb" in "/home/jmolson/conda/envs/DEEPLABCUT/lib/python3.8/site-packages/cv2/qt/plugins"
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.

And I wasted hours and couldn't fix it. Then I saw this:
https://forum.image.sc/t/dlc-2-3-0-fails-to-start-on-macos-12-6-2-tensorflow-or-qt-issue-how-to-resolve/75436/5
